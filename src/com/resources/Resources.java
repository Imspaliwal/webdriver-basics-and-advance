package com.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Resources {
	
	public static File f;
	public static FileInputStream FI;
	
	public static Properties Repository = new Properties();
	
	public static void Initialize() throws IOException {

		
	}
	
	// Load the Properties file
	public void loadPropertiesFile() throws IOException {
		f = new File("./Configuration/Config.properties");
		FI = new FileInputStream(f);
		Repository.load(FI);
	}
	
	public static String getUrl() {
		String appUrl = Repository.getProperty("url");
		return appUrl;
	}
}
