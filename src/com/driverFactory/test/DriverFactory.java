package com.driverFactory.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

	static WebDriver driver;

	public static WebDriver getBrowser(String browserName){

		if(browserName.toLowerCase().contains("chrome")) {
			System.setProperty("driver.chrome.driver", "C:\\Python27\\Scripts\\chromedriver.exe");
			driver = new ChromeDriver();
			//return driver;
		}

		return driver;
	}
}
