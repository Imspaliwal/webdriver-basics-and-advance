package com.propertyReader.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertyReader {

	public String readApplicationFile(final String key){
		String value = "";
		try {
			final Properties prop = new Properties();
			final File f = new File("C:\\eclipse-workspace\\MorningSessions\\Configuration\\Config.properties");

			if(f.exists()) {
				prop.load(new FileInputStream(f));
				value = prop.getProperty(key);
			}
		}
		catch (final Exception e) {
			System.out.println("---- Failed to read Config.properties file ----");
		}
		return value;
	}
	
	public String readTestData(final String key) {
		String value = "";
		try {
			final Properties prop = new Properties();
			final File f = new File("C:\\eclipse-workspace\\MorningSessions\\Configuration\\Config.properties");
			if (f.exists()) {
				prop.load(new FileInputStream(f));
				value = prop.getProperty(key);
			}
		} catch (final Exception e) {
			System.out.println("---- Failed to read Config.properties file ----");
		}
		return value;
	}
}

