package com.basic.test;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.driverFactory.test.DriverFactory;
import com.driverFactory.test.LocalDriverManager;
import com.propertyReader.test.PropertyReader;
import com.resources.Resources;

public class PropertyReaderTest extends Resources{
	
	WebDriver driver; 
	
	PropertyReader propertyReader = new PropertyReader();
	String brwType = propertyReader.readTestData("browser");

	@Test
	public void loginGmailTest() throws InterruptedException {
		LoginGmailPage login = new LoginGmailPage();
		login.login();
		
	}
	
	@BeforeTest
	public void initBrowser() throws IOException {
		Initialize();

	}
	
	@BeforeClass
	public void setup() throws IOException {
	
	driver = DriverFactory.getBrowser(brwType);
	LocalDriverManager.setWebDriver(driver);
	loadPropertiesFile();
	driver.get(getUrl());
	}
	
	@AfterMethod
	public void tearDown() throws IOException {
		
//		Take Screen Shot and store as a File Format.
		File src = ((TakesScreenshot) LocalDriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
		
//		Now copy the Screen Shot to desired location using copyFile method.
//		Has to import Ant utility --> http://commons.apache.org/proper/commons-io/download_io.cgi
//		FileUtils.copyFile(src, new File("C:\\eclipse-workspace\\MorningSessions\\TakeScreenShot\\freeCRMHome1.png"));
		
//		Now selenium supports following code:
		FileHandler.copy(src, new File("C:\\eclipse-workspace\\MorningSessions\\TakeScreenShot\\login.png"));
		
	}
	
	
	
	
	
}
