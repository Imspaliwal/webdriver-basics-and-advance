package com.basic.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.driverFactory.test.LocalDriverManager;
import com.resources.Resources;


public class LoginGmailPage extends Resources{
	
	public static WebDriver driver;

	//initiate web elements
	public LoginGmailPage(){
		PageFactory.initElements(LocalDriverManager.getDriver(), this);
	}


	/**
	 * Google login web elements
	 */

	@FindBy(linkText="Sign in")
//	@FindBy(xpath="//a[@class='gb_Pe gb_Ba gb_Mb']")
	public WebElement signInLink;

	@FindBy(xpath="//input[@type='email']")
	public WebElement email;

	@FindBy(id="identifierNext")
	public WebElement nextButton;

	@FindBy(xpath="//input[@type='password']")
	public WebElement password;

	@FindBy(id="passwordNext")
	public WebElement nextSubmit;

	@FindBy(className="gb_b gb_hc")
	public WebElement googleApps;

	@FindBy(linkText="Gmail")
	public WebElement gamilLink;


	//login method

	public void login() throws InterruptedException {
		signInLink.click();
		email.sendKeys("sumitpaliwal318");
		nextButton.click();

		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(password)));
		
		Thread.sleep(3000);

		password.sendKeys("123456");
		nextSubmit.click();

		Thread.sleep(3000);
	}
}