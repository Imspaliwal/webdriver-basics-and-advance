package com.basic.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class SetActiveWindow {
	
	static WebDriver driver;
	
	@Test
	public void browserWindowPopUpTest() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Python27\\Scripts\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("http://w2k2-app-pbart.ams.com:8080/PB01SERVER/Controller");
		
		driver.findElement(By.name("j_username")).sendKeys("admin");
		driver.findElement(By.name("j_password")).sendKeys("adminadmin");
		driver.findElement(By.className("ui-button-text")).submit();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//img[@alt='Home page in a new window']")).click();
		
		setActiveWindow();
//		
//		System.out.println(driver.getTitle());
	
		Thread.sleep(2000);
		
		driver.findElement(By.id("y5ev2rb")).click();
		
//		setActiveWindow();
		
		System.out.println(driver.getTitle());
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//img[@alt='Home page in a new window']")).click();
		
//		setActiveWindow();
//		
//		System.out.println(driver.getTitle());
		
		
	}
	
	public static void setActiveWindow() {

		int count = 0;
		for (String handle : driver.getWindowHandles()) { // Last
			if (count == driver.getWindowHandles().size() - 1) {
			//	int count1 = driver.getWindowHandles().size();
				driver.switchTo().window(handle);
				break;
			}
			count++;
		}
	}

}
