package com.basic.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.dataProvider.ExcelReader;

public class DataProviderTest {
	
	public static WebDriver driver;
	public static ExcelReader loginData;
	
	@Test
	public void findElementTest() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "C:\\Python27\\Scripts\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
		driver.get("http://w2k2-app-pbart.ams.com:8080/PB01SERVER/Controller");

		WebElement userName = driver.findElement(By.name("j_username"));
		WebElement password = driver.findElement(By.name("j_password"));
		
		loginData = new ExcelReader(System.getProperty("user.dir")+"//TestData//Login.xlsx");
		
		userName.sendKeys(loginData.getCellData("Login", "UserName", 2));
		Thread.sleep(3000);
		password.sendKeys(loginData.getCellData("Login", "Password", 2));
		Thread.sleep(3000);
		
		driver.close();
	}
}
