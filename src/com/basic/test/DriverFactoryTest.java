package com.basic.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.driverFactory.test.DriverFactory;

public class DriverFactoryTest {
	
	@Test
	public void driverFactoryTest() {
		WebDriver driver = DriverFactory.getBrowser("chrome");
		
		driver.get("https://www.goole.com");
		
		driver.close();
	}

}
